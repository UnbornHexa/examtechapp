<?php include 'includes/header.php'; ?>
<?php include 'includes/menu.php'; ?>
<!-- CSS -->
<link rel="stylesheet" href="assets/css/dashboard.css">
<!-- Content Header (Page header) -->
<section class="content-header" >
  <h1>Painel <small>Acesso rápido</small></h1>
  <ol class="breadcrumb">
    <li class="active">Painel</li>
  </ol>
</section>
<!-- Main content -->
<section class="content container-fluid">
<!--------------------------
  | Your Page Content Here |
  -------------------------->
    
    <!-- Info boxes -->
<div class="row">
  <!-- Box Saldo -->
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-green"><i class="ion ion-cash"></i></span>
      <div class="info-box-content">
        <span class="info-box-text"><b>Saldo</b></span>
        <span class="info-box-number" id="box-saldo"><!-- JQUERY --></span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
            <button type="button" class="btn btn-block btn-success btn-lg" id="btnSaldo" href="#modal-saldo" data-toggle="modal">Inserir Saldo</button>
            <button type="button" class="btn btn-block btn-success btn-lg" id="btnTransferencia" href="#modal-transferencia" data-toggle="modal">Fazer Transferência</button>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
    <br/>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="box" style="border-top-color: #008d4c;">
            <div class="box-header">
                <h3 class="box-title"><b>Extrato</b></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive" >
                <table id="extrato_table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Valor</th>
                            <th>Descrição</th>
                            <th>Tipo</th>
                            <th>Data</th>    
                        </tr>
                    </thead>
                    <!-- /.thead -->
                    <tbody id="extrato_body">
                        <!-- !!POPULAR COM JQUERY!!-->
                    </tbody>
                    <!-- /.tbody -->
                </table>
                <!-- /.table -->
                </div>
            <!-- /.box-body -->
            </div>
        </div>
    <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

<!-- Modal Inserir Saldo-->
<div class ="modal modal-default fade" id="modal-saldo" aria-hidden = "true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="/ExamTechApi/api/usuario/saldo/" method="post" id="formInserir">
        <div class ="modal-header" style="border-bottom-color: #008d4c;">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden = "true">X</span>
          </button>
          <h4 class ="modal-title">Inserir Saldo</h4>
        </div>
        <div class="modal-body">
          <p style = "text-align: center">Quanto de saldo deseja inserir em sua conta?</p>
                <!-- Valor -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="valor">Valor*</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-usd"></i>
                      </div>
                      <input type="text" class="form-control" id="valor" name="valor" required data-thousands="." data-decimal="," data-prefix="R$ " placeholder="R$ 0,00">
                    </div>
                  </div>
                </div>
            <!-- ./Valor -->
        </div>
          <!-- ./Modal-Body -->
        <div class = "modal-footer">
          <button type="submit" class="btn btn-success"> Inserir</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- ./Modal Inserir Saldo-->

<!-- Modal Realizar Transferência-->
<div class ="modal modal-default fade" id="modal-transferencia" aria-hidden = "true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="/ExamTechApi/api/movimentacao" method="post" id="formTransf">
        <div class ="modal-header" style="border-bottom-color: #008d4c;">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden = "true">X</span>
          </button>
          <h4 class ="modal-title">Realizar Transferência</h4>
        </div>
        <div class="modal-body">
          <p style = "text-align: center">Insira email de destin    atário e valor desejado.</p>
                <!-- Valor -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="valor">Valor*</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-usd"></i>
                      </div>
                      <input type="text" class="form-control" id="valor_transf" name="valor" placeholder="R$ 40,00" required value="" data-thousands="." data-decimal="," data-prefix="R$ " placeholder="R$ 0,00">
                    </div>
                  </div>
                </div>
            <!-- ./Valor -->
            <!-- E-mail -->
            <div class="col-md-6">
              <div class="form-group">
                <label for="email">Email*</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-envelope-o"></i>
                  </div>
                  <input type="email" class="form-control" id="email_transf" name="email" placeholder="usuario@dominio.com" value="">
                </div>
              </div>
            </div>
            <!-- ./E-mail -->
            <!-- Descricao -->
            <div class="col-md-12">
              <div class="form-group">
                <label for="descricao">Descrição</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-bold"></i>
                  </div>
                    <input type="text" class="form-control" id="descricao_transf" name="descricao" placeholder="Descrição da Transação" value="">
                </div>
              </div>
            </div>
            <!-- ./Descricao -->
        </div>
        <div class = "modal-footer">
          <button type="submit" class="btn btn-success"> Transferir</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- ./Modal Realizar Transferência-->
<?php include 'includes/footer.php' ; ?>
<!-- JS -->
<!-- DataTables -->
<script src="vendor/almasaeed2010/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vendor/almasaeed2010/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="assets/js/dashboard.js"> </script>