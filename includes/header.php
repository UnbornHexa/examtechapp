<!DOCTYPE html>
<html lang="pt-br">

<!--Header-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta id="descricao" name="descricao" content="">
  <meta name="author" content="Irmãos Orozco">

  <!--Theme Color-->
  <meta name="theme-color" content="#605ca8">
  <meta name="msapplication-navbutton-color" content="#605ca8">
  <meta name="apple-mobile-web-app-status-bar-style" content="#605ca8">

  <title id = "title_header"></title>
  
  <!-- CSS -->
  <link rel="stylesheet" href="vendor/almasaeed2010/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="vendor/almasaeed2010/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="vendor/almasaeed2010/adminlte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="vendor/almasaeed2010/adminlte/dist/css/skins/skin-green.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<!--Body-->
<body class="hold-transition skin-green sidebar-mini" ondragstart="return false;" ondrop="return false;">