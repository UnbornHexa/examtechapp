<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Main Footer -->
<footer class="main-footer text-center">
	<!-- Default to the left -->
	<strong>Copyright &copy; <?php echo date('Y'); ?> Irmãos Orozco</strong>
</footer>
</div>
<!-- ./wrapper -->
<!--Javascript-->
<!-- jQuery 3 -->
<script src="vendor/almasaeed2010/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- MaskMoney -->
<script src="vendor/jquery-maskmoney-master/dist/jquery.maskMoney.min.js" type="text/javascript"></script>
<!-- Bootstrap 3.3.7 -->
<script src="vendor/almasaeed2010/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="vendor/almasaeed2010/adminlte/dist/js/adminlte.min.js"></script>
<!-- Slimscroll -->
<script src="vendor/almasaeed2010/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="vendor/almasaeed2010/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
</body>
</html>