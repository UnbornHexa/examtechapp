<?php include 'includes/header.php' ?>
<?php include 'includes/menu.php' ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Perfil <small>Perfil do Usuário</small></h1>
  <ol class="breadcrumb">
    <li class="active">Perfil</li>
  </ol>
</section>
<!-- Main content -->
<section class="content container-fluid">
  <!--------------------------
    | Your Page Content Here |
    -------------------------->
  
  <div class="box box-success">
    <!-- box-header -->
    <div class="box-header with-border">
      <h3 class="box-title"> Dados do Perfil</h3>
    </div>
    <!-- /.box-header -->
    <!-- box-body -->
    <div class="box-body">
      <div class="col-md-6">  
        <form role="form" action="/ExamTechApi/api/usuario/" id="form_conta" method="put">
          <!-- Nome -->
        <div class="form-group">
          <label for="nome">Nome</label>
          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-user"></i>
            </div>
            <input type="text" class="form-control" id="nome" name="nome" placeholder="Wesley Silva" value="">
          </div>
        </div>
        <!-- ./Nome -->
        <!-- Email -->
        <div class="form-group">
          <label for="email">Email</label>
          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-envelope-o"></i>
            </div>
            <input type="email" class="form-control" id="email" name="email" placeholder="usuario@dominio.com" value="" >
          </div>
        </div>
        <!-- ./Email -->
          <!-- Senha -->
          <div class="form-group">
            <label for="senha_atual">Senha Atual*</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-unlock-alt"></i>
              </div>
              <input type="password" class="form-control" id="senha_atual" name="senha_atual" placeholder="******" required>
            </div>
          </div>
          <!-- Senha Nova -->
          <div class="form-group">
            <label for="senha_nova">Senha Nova*</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-unlock-alt"></i>
              </div>
              <input type="password" class="form-control" id="senha_nova" name="senha_nova" placeholder="******" required>
            </div>
          </div>
          <!-- Button -->
          <div class="form-group">
            <button type="submit" class="btn btn-success" id="editar_conta" name="editar_conta">Salvar</button>
          </div>
        </form>
          <button type="button" class="btn btn-danger" id="deletar_conta" name="deletar_conta" data-toggle="modal" href="#modal-deletar">Deletar Conta!</button>
        <br>
      </div>
      <!-- / Coluna 2 -->
    <!-- /.box-body -->
    <!-- box-footer -->
    <div class="box-footer">
    </div>
    <!-- /.box-footer -->
  </div>
  <!-- /.box -->
</section>
    
    <!-- Modal Deletar Conta-->
<div class ="modal modal-danger fade" id="modal-deletar" aria-hidden = "true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class ="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden = "true">X</span>
          </button>
          <h4 class ="modal-title">Excluir Conta</h4>
        </div>
        <div class="modal-body">
          <p style = "text-align: center; font-size:20px;"><b>Você deseja mesmo excluir essa Conta?</b></p>
        </div>
        <div class = "modal-footer">
          <button type="button" class="btn btn-warning btn-outline" id="btnDeletar">Excluir</button>
        </div>
    </div>
  </div>
</div>
<!-- ./Modal Deletar Conta-->
<?php include 'includes/footer.php' ?>
<!-- JS -->
<script src="assets/js/perfil.js"> </script>
<script src="vendor/autoload.php"></script>