<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta id="descricao" name="descricao" content="">
  <meta name="author" content="Irmãos Orozco">

  <!--Theme Color-->
  <meta name="theme-color" content="#605ca8">
  <meta name="msapplication-navbutton-color" content="#605ca8">
  <meta name="apple-mobile-web-app-status-bar-style" content="#605ca8">
    
  <title id="title_header"></title>
  <?php include 'assets/css/css.html'; ?>
</head>
<body id="grad">
  <div class="form">
      
      <ul class="tab-group">
        <li class="tab"><a href="#registrar">Registrar</a></li>
        <li class="tab active"><a href="#entrar">Entrar</a></li>
      </ul>
      
      <div class="tab-content">

         <div id="entrar">   
          <h1>Bem-Vindo de Volta!</h1>
          
          <form action="/ExamTechApi/api/token" method="post" autocomplete="off" id="entrarForm">
          
            <div class="field-wrap">
            <label>
              Login<span class="req">*</span>
            </label>
            <input type="text" required autocomplete="off" name="login" id="login" autofocus/>
          </div>
          
          <div class="field-wrap">
            <label>
              Senha<span class="req">*</span>
            </label>
            <input type="password" required autocomplete="off" name="senha" id="senha"/>
          </div>
          
          <button type="submit" class="button button-block" name="entrar" id="btnEntrar" />Entrar</button>
          
          </form>

        </div>
          
        <div id="registrar">   
          <h1>Registre-se!</h1>
          
          <form action="/ExamTechApi/api/usuario" method="post" autocomplete="off" id="registrarForm">
          
          <div class="top-row">
            <div class="field-wrap">
              <label>
                Nome<span class="req">*</span>
              </label>
              <input type="text" required autocomplete="off" name='nome' id="nome_registro"/>
            </div>
        
            <div class="field-wrap">
              <label>
                Login<span class="req">*</span>
              </label>
              <input type="text"required autocomplete="off" name='login' id="login_registro"/>
            </div>
          </div>

          <div class="field-wrap">
            <label>
              Email<span class="req">*</span>
            </label>
            <input type="email"required autocomplete="off" name='email' id="email_registro" />
          </div>
          
          <div class="field-wrap">
            <label>
              Senha<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" name='senha' id="senha_registro"/>
          </div>
              
          <div class="field-wrap">
            <label>
              Confirma Senha<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" name='confirma' id="confirma_senha"/>
          </div>
          
          <button type="submit" class="button button-block" name="registrar" id="btn_registrar"/>Registrar</button>
          
          </form>

        </div>  
        
      </div><!-- tab-content -->
      
</div> <!-- /form -->
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script src="assets/js/index.js"></script>

</body>
</html>
