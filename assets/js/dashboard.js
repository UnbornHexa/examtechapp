//Abre Menu Lateral
$('#menu-lateral-dashboard').addClass("active");
    //Recebe as variaveis pelo localstorage
   var login_usuario = localStorage.getItem('login'); 
   var id_usuario = localStorage.getItem('id_usuario') ;
    //Grava o token na localstorage
    var token = localStorage.getItem('token');

$(document).ready(function(){
    //Altera descrição e titulo da pagina dinamicamente
   $('#descricao').attr('content','Página inicial do painel');
    $('#title_header').html('Página Inicial - Exam Tech');
    //Impede entrar pela url
    if(token == null){
        window.location.href='index.php';
    }
    //Altera o nome do usuario e login na tela
    document.getElementById('nome_usuario').innerHTML = login_usuario;
     document.getElementById('login').innerHTML = login_usuario;
    //Mascara de Input de dinheiro
    $('#valor').maskMoney();
    $('#valor_transf').maskMoney();
    //Ajax para povoar o Saldo
    $.ajax({
        type: 'GET',
        url: '/ExamTechApi/api/usuario/saldo/'+id_usuario,
        success:function(saldo){
            var _saldo = JSON.parse(saldo);
            var saldo_final = 'R$ ' + Number(_saldo).toFixed(2).replace('.', ',');
            document.getElementById('box-saldo').innerHTML = saldo_final;
        }
        
    });
    
    //Ajax para povoar a tabela de Extrato
    $.ajax({
        type:'GET',
        url:'/ExamTechApi/api/movimentacao/' +id_usuario,
        success:function(extrato){
            var _extrato = JSON.parse(extrato) ;
            $.each(_extrato,function(i, extrato){
                var tipo_movimentacao = null;
                if(extrato.tipo_movimentacao == 1){
                    tipo_movimentacao = "Depósito";
                    var classe = "tipo_deposito";
                }
                else{ tipo_movimentacao = "Transferência";
                     var classe = "tipo_transf"}
                var date = new Date(extrato.data_movimentacao);
               $('#extrato_body').append(
                   '<tr>'+
                   '<td>'+'R$ ' + Number(extrato.vl_movimentacao).toFixed(2).replace('.', ',')+'</td>'+
                   '<td>'+extrato.descricao_movimentacao+'</td>'+
                   '<td>'+'<span class="'+classe+'">'+tipo_movimentacao+'</span></td>'+
                   '<td>'+date.toLocaleDateString('pt-BR')+'</td>'
                   +'</tr>'
               ); 
            });
        }
    });
});


//Ajax para Inserir Saldo
$('#formInserir').on('submit',function(e){
    e.preventDefault();
    var _url = $(this).attr('action')+id_usuario;
    $('#valor');
    var valor = Number($('#valor').val().replace('R$ ', '').replace('.','').replace(',', '.'));
    //Ajax para Inserir Saldo
    $.ajax({
        type: 'PUT' ,
        url: _url,
        data: {valor:valor},
        success:function(dataResultado){
            var _data = JSON.parse(dataResultado);
            alert(_data.mensagem);
            window.location.href="dashboard.php";
        },
        error:function(){
            alert('Erro na inserção de saldo');
        }
    });
    
});

//Ajax para Realizar Transferencia
$('#formTransf').on('submit',function(e){
    e.preventDefault();
    var descricao = $('#descricao_transf').val();
    var valor = Number($('#valor_transf').val().replace('R$ ', '').replace('.','').replace(',', '.'));
    var email = $('#email_transf').val();
    //Ajax para Inserir Saldo
    $.ajax({
        type: 'POST' ,
        url: $(this).attr('action'),
        data: {vl_movimentacao:valor,
               descricao_movimentacao:descricao,
               tipo_movimentacao:0,
               Usuario_id_usuario:id_usuario,
               email_destinatario:email
              },
        success:function(dataResultado){
            var _data = JSON.parse(dataResultado);
            alert(_data.mensagem);
            window.location.href="dashboard.php";
        },
        error:function(){
            alert('Erro na inserção de saldo');
        }
    });
    
});




