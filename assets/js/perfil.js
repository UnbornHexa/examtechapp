$('#menu-lateral-dashboard').addClass("active");
    //Recebe as variaveis pelo localstorage
   var login_usuario = localStorage.getItem('login'); 
   var id_usuario = localStorage.getItem('id_usuario') ;
    //Grava o token na localstorage
    var token = localStorage.getItem('token');

$(document).ready(function(){
    //Altera descrição e titulo da pagina dinamicamente
   $('#descricao').attr('content','Página de Conta');
    $('#title_header').html('Minha Conta - Exam Tech');
    //Impede entrar pela url
    if(token == null){
        window.location.href='index.php';
    }
    //Altera o nome do usuario e login na tela
    document.getElementById('nome_usuario').innerHTML = login_usuario;
     document.getElementById('login').innerHTML = login_usuario;
    
    $.ajax({
        type: 'GET',
        url: '/ExamTechApi/api/usuario/'+id_usuario,
        success:function(data){
            var _data = JSON.parse(data);
            document.getElementById('nome').value = _data['nm_usuario'];
            document.getElementById('email').value = _data['email_usuario'];
        },
       error:function(){
        alert('Erro ao carregar dados');
    } 
    });
});

$('#form_conta').on('submit',function(e){
    e.preventDefault();
    var nome = $('#nome').val();
    var email = $('#email').val();
    var senha = $('#senha').val();
    var senha_nova = $('#senha_nova').val();
    var _url = $(this).attr('action')+id_usuario;
    if(senha_nova != senha){
        senha = senha_nova;
    }
    let data = {
        "nm_usuario": nome,
        "email_usuario": email,
        "senha_usuario": senha
    }
    console.log(data);
    $.ajax({
        type:'PUT',
        url: _url,
        data: data,
        success: function(dataResultado){
            var resultado = JSON.parse(dataResultado);
            
            if(resultado.isError == false){
             alert(resultado.mensagem);
                window.location.href="perfil.php";
            }
        },
        error:function(){
            alert('Erro na alteração de perfil');
        }
    });
});

$('#btnDeletar').on('click', function(){
    var _url = '/ExamTechApi/api/usuario/'+id_usuario;
   $.ajax({
     type: 'DELETE',
     url: _url,
       success:function(dataResultado){
           var resultado = JSON.parse(dataResultado);
           console.log(resultado);
           if(resultado.isError == false){
              alert(resultado.mensagem);
               localStorage.clear();
               window.location.href="index.php";
              }
           else{
               alert(resultado.mensagem);
           }
       },
       error:function(){
           alert('Erro na exclusão de conta');
       }
   });
});