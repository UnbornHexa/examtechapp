var $login = $('#login');
var $senha = $('#senha');

$('.form').find('input, textarea').on('keyup blur focus', function (e) {
  
  var $this = $(this),
      label = $this.prev('label');

	  if (e.type === 'keyup') {
			if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
    	if( $this.val() === '' ) {
    		label.removeClass('active highlight'); 
			} else {
		    label.removeClass('highlight');   
			}   
    } else if (e.type === 'focus') {
      
      if( $this.val() === '' ) {
    		label.removeClass('highlight'); 
			} 
      else if( $this.val() !== '' ) {
		    label.addClass('highlight');
			}
    }

});

$('.tab a').on('click', function (e) {
  
  e.preventDefault();
  
  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');
  
  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();
  
  $(target).fadeIn(600);
  
});

$(document).ready(function(){
   $('#descricao').attr('content','Página de Login');
    $('#title_header').html('Login - Exam Tech');
    var token = localStorage.getItem('token');
    if(token != null){
        window.location.href='dashboard.php';
    }
});


$('#entrarForm').on('submit',function(e){
        e.preventDefault(); 
        var autent = {
            login_usuario: $login.val(),
            senha_usuario: $senha.val(),
            }
        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data: autent,
            success:function(dataResultado){
                var resultado = JSON.parse(dataResultado);
                
                if(resultado.isError == false){    
                localStorage.setItem('token',resultado.token);
                 localStorage.setItem('id_usuario',resultado.id_usuario_logado);
                 localStorage.setItem('login',resultado.login_usuario);
                window.location.href='dashboard.php';
                }
                else{
                    alert(resultado.mensagem);
                }
                    
            },
            error:function(){
                alert('Erro na autenticação');
            }
        });
    });

$('#registrarForm').on('submit',function(e){
   e.preventDefault();
    var nome = $('#nome_registro').val();
    var login = $('#login_registro').val();
    var email = $('#email_registro').val();
    var senha = $('#senha_registro').val();
    var confirma = $('#confirma_senha').val();
    if(senha == confirma){
        var registro = {
            nm_usuario: nome,
            email_usuario: email,
            login_usuario: login,
            senha_usuario: senha
       }
        var autent = {
            login_usuario: $login.val(),
            senha_usuario: $senha.val(),
            }
        $.ajax({
           type:'POST' ,
            url: $(this).attr('action'),
            data: registro,
            success:function(dataResultado){
                var resultado = JSON.parse(dataResultado);
                console.log(resultado);
                if(resultado.isError == false){    
                window.location.href='index.php';
                }
                /*else{
                    alert(resultado.mensagem);
                }*/
            }
        });
    }
    else{alert('Senha e Confirmação devem ser iguais');}
});